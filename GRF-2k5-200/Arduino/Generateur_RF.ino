#include <Wire.h>
#include <Adafruit_SI5351.h>
#include <LiquidCrystal.h>

#include <Encoder.h>

// #define DEBUG_FREQ_CALC
#define DEBUG_SWEEP

#define MENU_F     0
#define MENU_F_DEB 1
#define MENU_F_FIN 2
#define MENU_PAS   3
#define MENU_MODE  4
#define MENU_SOR   5



#define CODER_INTER 3

#define XTAL_FREQ 27000000              // Crystal frequency for Si5351A board - needs to be adjusted specific to your board's crystal
#define TEMPS_DELAY 1000

Adafruit_SI5351 generateur = Adafruit_SI5351();
LiquidCrystal lcd(13, 12, 11, 10, 9, 8);
Encoder myEnc(5, 6);

bool sortie = false;
bool sweep = false;

long tab_pow[9] = {100e6, 10e6, 1e6, 100e3, 10e3, 1e3, 1e2, 1e1, 1e0};
long freq_deb = 100e6;
long freq_fin = 100.05e6;
long freq_pas = 10000.0;
long frequence = 100e6;

int32_t next_sweep = 0;
int32_t temps_sw;
int32_t temp_bas;
int32_t temps_loop;

bool menu = false;
bool valide = false;
int  pos_menu = 0;   /// 0:freq 1:f_deb 2:f_fin 3:mode 4:sortie
long old_coder = 0;

int menu_lig[6] = {0, 1, 2, 1, 3, 3};
int menu_col[6] = {0, 0, 0, 12, 0, 15};

byte customChar[8] = {
	0b00100,
	0b01110,
	0b11111,
	0b00100,
	0b00100,
	0b00100,
	0b00100,
	0b00100
};

/**************************************************************************/
/*
    Arduino setup function (automatically called at startup)
*/
/**************************************************************************/
void setup(void)
{
  Serial.begin(9600);
  Serial.print("Si5351 Clockgen Test...\t");

  lcd.begin(16, 4);
  lcd.print("Si5351 Clockgen Test...\t");
  lcd.createChar(0, customChar);
  
  /* Initialise the sensor */
  if (generateur.begin() != ERROR_NONE)
  {
    /* There was a problem detecting the IC ... check your connections */
    Serial.print("Ooops, no Si5351 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }
  Serial.println("OK!");
  lcd.print("OK!");
  lcd.clear();
  lcd_maj();

  pinMode(CODER_INTER, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(CODER_INTER), int_coder, CHANGE);
  temps_loop = millis();
}

/**************************************************************************/
/*
    Arduino loop function,
*/
/**************************************************************************/
void loop(void)
{
  String instruction;
  String valeur;

  if (Serial.available() > 0)
  {
    instruction = Serial.readStringUntil(':');
    if (instruction == "FREQ" || instruction == "freq" || instruction == "F" || instruction == "f")
    {
      valeur = Serial.readStringUntil(':');
      frequence = valeur.toFloat();
      Serial.print("freq utilisateur : ");
      Serial.println(frequence);
      Serial.println(" MHz");
      setFrequence(frequence);
    }
    else if (instruction == "SWEEP" || instruction == "sweep" || instruction == "sw" || instruction == "SW")
    {
      valeur = Serial.readStringUntil(':');
      freq_deb = valeur.toFloat();
      valeur = Serial.readStringUntil(':');
      freq_fin = valeur.toFloat();
      valeur = Serial.readStringUntil(':');
      freq_pas = valeur.toFloat();

      if (freq_deb == 0 || freq_fin == 0 || freq_pas == 0)
        sweep = false;
      else
      {
        sweep = true;
        frequence = freq_deb;
        if (freq_deb > freq_fin)
        {
          frequence = freq_fin;
          freq_fin = freq_deb;
          freq_deb = frequence;
        }
        next_sweep = millis() + 1000;
        setFrequence(frequence);
      }
#ifdef DEBUG_SWEEP
      Serial.print("freq deb : ");
      Serial.println(freq_deb);
      Serial.print("freq fin : ");
      Serial.println(freq_fin);
      Serial.print("freq pas : ");
      Serial.println(freq_pas);
      Serial.print("freq  : ");
      Serial.println(frequence);
#endif
    }

    else if (instruction == "ON" || instruction == "on")
    {
      generateur.enableOutputs(true);
      Serial.println("Sortie : ON");
      sortie = true;
    }
    else if (instruction == "OFF" || instruction == "off")
    {
      generateur.enableOutputs(false);
      Serial.println("Sortie : OFF");
      sortie = false;
    }
    else
    {
      Serial.print("ERR : instruction inconnue");
    }
    lcd_maj();
  }
  ///////////////////// TODO gestion du sweep
  if ( sortie && sweep && (millis() - temps_sw) > TEMPS_DELAY)
  {
    frequence += freq_pas;
    if (frequence > freq_fin)
      frequence = freq_deb;
    Serial.print("freq sw : ");
    Serial.println(frequence);
    setFrequence(frequence);
    lcd_maj();
    temps_sw = millis();
  }
  ////////////////// CODER
  if (menu && (temps_loop - millis() > 500))
  {
    long newPosition = myEnc.read() / 2;

    pos_menu = pos_menu + (old_coder - newPosition);
    if (pos_menu < 0)
      pos_menu = 5;
    else if (pos_menu > 5)
      pos_menu = 0;

    lcd.setCursor(menu_col[pos_menu], menu_lig[pos_menu]);
    old_coder = newPosition;
    temps_loop = millis();
  }

  long pos_org;
  long pos_cod;
  long myEnc_origine;
  int  cursor_pos = 9;
  int  tab_freq[9];
  long frequence_tmp;
  int i;
  if (valide && (temps_loop - millis() > 200))
  {
    switch (pos_menu)
    {
      case 0 :
        Serial.println("Frequ");
        pos_org = myEnc.read() / 2;
        myEnc_origine = myEnc.read();
        valide = false;
        frequence_tmp = frequence;
        // copie de la frequence dans un tableau 
        for (cursor_pos = 0; cursor_pos < 9; cursor_pos++) {
          tab_freq[cursor_pos] = frequence_tmp / tab_pow[cursor_pos];
          frequence_tmp = frequence_tmp - (tab_freq[cursor_pos] * tab_pow[cursor_pos]);
        }
        // deplacement dans les digit de la frequence
        for (cursor_pos = 0; cursor_pos < 9 ; cursor_pos++) {
          pos_org = myEnc.read() / 2;
          do {
            lcd.setCursor(cursor_pos + 2,menu_lig[0]);
            pos_cod = myEnc.read() / 2;
            tab_freq[cursor_pos] = tab_freq[cursor_pos] + (pos_org - pos_cod);
            if (tab_freq[cursor_pos] > 9)
              tab_freq[cursor_pos] = 0;
            else if (tab_freq[cursor_pos] < 0)
              tab_freq[cursor_pos] = 9;
           
            frequence = 0.0;
            for (i = 0; i < 9; i++) {
              frequence = frequence + long(tab_freq[i]) * tab_pow[i];
            }
             Serial.println(frequence);
          
            if((pos_org - pos_cod) != 0)
              lcd_maj();
            pos_org = pos_cod;
          } while (!valide);
         valide = false;
        setFrequence(frequence);
        }
        myEnc.write(myEnc_origine);
        break;

      case 1 :
        Serial.println("Frequ deb");
        pos_org = myEnc.read() / 2;
        myEnc_origine = myEnc.read();
        valide = false;
        frequence_tmp = freq_deb;
        // copie de la frequence dans un tableau 
        for (cursor_pos = 0; cursor_pos < 6; cursor_pos++) {
          tab_freq[cursor_pos] = frequence_tmp / tab_pow[cursor_pos];
          frequence_tmp = frequence_tmp - (tab_freq[cursor_pos] * tab_pow[cursor_pos]);
        }
        // deplacement dans les digit de la frequence
        for (cursor_pos = 0; cursor_pos < 6 ; cursor_pos++) {
          pos_org = myEnc.read() / 2;
          do {
            lcd.setCursor(menu_col[MENU_F_DEB] + cursor_pos + 4, menu_lig[MENU_F_DEB]);
            pos_cod = myEnc.read() / 2;
            tab_freq[cursor_pos] = tab_freq[cursor_pos] + (pos_org - pos_cod);

            if (tab_freq[cursor_pos] > 9)
              tab_freq[cursor_pos] = 0;
            else if (tab_freq[cursor_pos] < 0)
              tab_freq[cursor_pos] = 9;

            freq_deb = 0.0;
            for (i = 0; i < 6; i++) {
              freq_deb = freq_deb + long(tab_freq[i]) * tab_pow[i];
            }
            Serial.println(freq_deb);
            if((pos_org - pos_cod) != 0)
              lcd_maj();
            pos_org = pos_cod;
          } while (!valide);
         valide = false;
        }
        myEnc.write(myEnc_origine);
        break;
                
      case 2 :
        Serial.println("Frequ fin");
        pos_org = myEnc.read() / 2;
        myEnc_origine = myEnc.read();
        valide = false;
        frequence_tmp = freq_fin;
        // copie de la frequence dans un tableau 
        for (cursor_pos = 0; cursor_pos < 6; cursor_pos++) {
          tab_freq[cursor_pos] = frequence_tmp / tab_pow[cursor_pos];
          frequence_tmp = frequence_tmp - (tab_freq[cursor_pos] * tab_pow[cursor_pos]);
        }
        // deplacement dans les digit de la frequence
        for (cursor_pos = 0; cursor_pos < 6 ; cursor_pos++) {
          pos_org = myEnc.read() / 2;
          do {
            lcd.setCursor(menu_col[MENU_F_FIN] + cursor_pos + 4, menu_lig[MENU_F_FIN]);
            pos_cod = myEnc.read() / 2;
            tab_freq[cursor_pos] = tab_freq[cursor_pos] + (pos_org - pos_cod);

            if (tab_freq[cursor_pos] > 9)
              tab_freq[cursor_pos] = 0;
            else if (tab_freq[cursor_pos] < 0)
              tab_freq[cursor_pos] = 9;

            freq_fin = 0.0;
            for (i = 0; i < 6; i++) {
              freq_fin = freq_fin + long(tab_freq[i]) * tab_pow[i];
            }
            Serial.println(freq_fin);
            if((pos_org - pos_cod) != 0)
              lcd_maj();
            pos_org = pos_cod;
          } while (!valide);
         valide = false;
        }
        myEnc.write(myEnc_origine);
        break;
      case 3 :
        Serial.println("pas");
        pos_org = myEnc.read() / 2;
        myEnc_origine = myEnc.read();
        valide = false;
        frequence_tmp = freq_pas;
        // copie de la frequence dans un tableau 
        for (cursor_pos = 2; cursor_pos < 6; cursor_pos++) {
          tab_freq[cursor_pos-2] = frequence_tmp / tab_pow[cursor_pos];
          frequence_tmp = frequence_tmp - (tab_freq[cursor_pos-2] * tab_pow[cursor_pos]);
        }
        // deplacement dans les digit de la frequence
        for (cursor_pos = 2; cursor_pos < 6 ; cursor_pos++) {
          pos_org = myEnc.read() / 2;
          do {
            lcd.setCursor(menu_col[MENU_PAS] + cursor_pos - 2, menu_lig[MENU_PAS] + 1);
            pos_cod = myEnc.read() / 2;
            tab_freq[cursor_pos-2] = tab_freq[cursor_pos-2] + (pos_org - pos_cod);

            if (tab_freq[cursor_pos-2] > 9)
              tab_freq[cursor_pos-2] = 0;
            else if (tab_freq[cursor_pos-2] < 0)
              tab_freq[cursor_pos-2] = 9;

            freq_pas = 0.0;
            for (i = 0; i < 4; i++) {
              freq_pas = freq_pas + long(tab_freq[i]) * tab_pow[i+2];
            }
            Serial.println(freq_pas);
            if((pos_org - pos_cod) != 0)
              lcd_maj();
            pos_org = pos_cod;
          } while (!valide);
         valide = false;
        }
        myEnc.write(myEnc_origine);
        break;
        
      case 4 :
        Serial.println("swp");
        sweep = !sweep;
        valide = false;
        break;
      
      case 5 :
        Serial.println("sortie");
        if (sortie)
          generateur.enableOutputs(false);
        else
          generateur.enableOutputs(true);
        sortie = !sortie;
        valide = false;
        break;
    }
    lcd_maj();
  }
  if (temp_bas > 0)
  {
    if (millis() - temp_bas > 1000)
    {
      Serial.println("CLIC LONG");
      menu = !menu;
      if (menu)
        //lcd.cursor();
        lcd.blink();
      else
        //lcd.noCursor();
        lcd.noBlink();
      temp_bas = -1;
    }
  }
}

//// CODER
void int_coder()
{

  if (!digitalRead(CODER_INTER))
  {
    temp_bas = millis();
  }
  else if (digitalRead(CODER_INTER))
  {
    if ((millis() - temp_bas > 100) && (millis() - temp_bas < 1000))
    {
      Serial.println("CLIC COURT");
      if (menu)
        valide = true;
    }
    temp_bas = -1;
  }

  //Serial.println("CODER");
}

/// Mise a jour de l'affichage du lcd
void lcd_maj()
{
  int i;
  long frequence_tmp = frequence;
  int digit;
  
  //// Frequence
  lcd.setCursor(menu_col[MENU_F], menu_lig[MENU_F]);
  lcd.print("F ");
  //lcd.setCursor(menu_col[MENU_F] + 2, menu_lig[MENU_F]);
  for (i = 0; i < 9; i++) {
          digit = int(frequence_tmp / tab_pow[i]);
          frequence_tmp = frequence_tmp - (long(digit) * tab_pow[i]);
          lcd.print(digit);
        }
  if(frequence > 150e6)
  {
    lcd.print(" Hz ");
      lcd.write((uint8_t)0);
  }
  else
      lcd.print(" Hz  ");

  //// Frequence debut sweep
  lcd.setCursor(menu_col[MENU_F_DEB], menu_lig[MENU_F_DEB]);
  lcd.print("Deb ");
  frequence_tmp = freq_deb;
  for (i = 0; i < 6; i++) {
          digit = int(frequence_tmp / tab_pow[i]);
          frequence_tmp = frequence_tmp - (long(digit) * tab_pow[i]);
          lcd.print(digit);
        }
  lcd.print("k");
 
  //// Frequence fin sweep
  lcd.setCursor(menu_col[MENU_F_FIN], menu_lig[MENU_F_FIN]);
  lcd.print("Fin ");
  frequence_tmp = freq_fin;
  for (i = 0; i < 6; i++) {
          digit = int(frequence_tmp / tab_pow[i]);
          frequence_tmp = frequence_tmp - (long(digit) * tab_pow[i]);
          lcd.print(digit);
        }
  lcd.print("k");
 
  //// Pas sweep
  lcd.setCursor(menu_col[MENU_PAS], menu_lig[MENU_PAS]);
  lcd.print("Pas");
  lcd.setCursor(menu_col[MENU_PAS], menu_lig[MENU_PAS] + 1);
  frequence_tmp = freq_pas;
  for (i = 2; i < 6; i++) {
          digit = int(frequence_tmp / tab_pow[i]);
          frequence_tmp = frequence_tmp - (long(digit) * tab_pow[i]);
          lcd.print(digit);
        }
 
  //lcd.print(freq_pas / 1e3);


  //// Mode
  lcd.setCursor(menu_col[MENU_MODE], menu_lig[MENU_MODE]);
  if (sweep)
    lcd.print("SWP");
  else
    lcd.print("CW ");

  //// Sortie
  lcd.setCursor(menu_col[MENU_SOR] - 9, menu_lig[MENU_SOR]);
  lcd.print("Sortie : ");
  if (sortie)
    lcd.print("X");
  else
    lcd.print("O");
}




void setFrequence(long frequency)
{
  uint32_t pllFreq;
  uint32_t l;
  float f;
  uint8_t mult;
  uint32_t num;
  uint32_t denom;
  uint32_t divider;

#ifdef  DEBUG_FREQ_CALC
  double freq_test;
#endif

  divider = 900000000 / frequency;        // Calculate the division ratio. 900,000,000 is the maximum internal PLL frequency: 900MHz
  if (divider % 2) divider--;        // Ensure an even integer division ratio

  pllFreq = divider * frequency;      // Calculate the pllFrequency: the divider * desired output frequency

  mult = pllFreq / XTAL_FREQ;          // Determine the multiplier to get to the required pllFrequency
  l = pllFreq % uint32_t(XTAL_FREQ);                 // It has three parts:
  f = l;                              // mult is an integer that must be in the range 15..90

  f *= 1048575;                       // num and denom are the fractional parts, the numerator and denominator
  f /= XTAL_FREQ;                      // each is 20 bits (range 0..1048575)
  num = f;                            // the actual multiplier is mult + num / denom
  denom = 1048575;                    // For simplicity we set the denominator to the maximum 1048575

#ifdef DEBUG_FREQ_CALC
  Serial.print("deviseur : ");
  Serial.println(divider);
  Serial.print("PLLfreq : ");
  Serial.println(pllFreq);
  Serial.print("multi : ");
  Serial.println(mult);
  Serial.print("f : ");
  Serial.println(f);
  Serial.print("num : ");
  Serial.println(num);
  Serial.print("denum : ");
  Serial.println(denom);
#endif

  generateur.setupPLL(SI5351_PLL_A, mult, num, denom);
  Serial.print("Set Output #0 : ");
  Serial.println((XTAL_FREQ * (mult + (float(num) / float(denom)))) / divider);
  generateur.setupMultisynth(0, SI5351_PLL_A, divider, 0, 1);
  if (sortie == true) generateur.enableOutputs(true);
}



 # Generateur RF
By F4HGD

## GRF-2k5-200
### Matériel 
 * Arduino (uno, nano...)
 * Si5351 (utilisation du module QRP-Labs.com)
 * Afficheur LCD 4 lignes
 * Encodeur rortatif

### Logiciel
En cours de dévellopement 

## GRF-34-4400
### Mariel
 * Arduino nano
 * ADF4350 PLL 34MHz-4400MHz
 * PE4302  Attenuateur 31,5 dB
 * Ampli 50MHz-6GHz
 * TFT 
 * Encoder rotatif

## Logiciel utilisé
### Schema et PCB 
 - Kicad

### Dev Arduino
 - IDE arduino
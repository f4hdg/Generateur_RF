/**********************************
 * Lib pour ADF4350
 * part Loic F4HDG
 * 
 * creation par le mixage des codes
 *   de Neal Pisenti, 2013. JQI - Strontium - UMD
 *   et Alain Fort F1CJN feb 2,2016
 * un grand merci a eux
 ***********************************/

#ifndef LibADF4350_h
#define LibADF4350_h

#include "Arduino.h"

class ADF4350
{
    public: 
        // Constructor function. 
        // Creates PLL object, with given SS pin
        ADF4350(int);

        // Initialize with initial frequency, refClk (defaults to 10Mhz); 
        void initialize(double, double);
        void rfEnable(bool);
        void setRfPower(int);
		void setFreq(double);
        
    private:
        int      slaveSelectPin;  //SPI-SS bzw. enable ADF4350
        uint32_t registers[6] =  {0x4580A8, 0x80080C9, 0x4E42, 0x4B3, 0xBC803C, 0x580005} ;
        double   RFfreq, PFDRFfreq, OutputChannelSpacing;
        int      RFpow;
        // D2 D1 RFpow OUTPUT POWER
        // 0  0   0      -4
        // 0  1   1      -1
        // 1  0   2      +2
        // 1  1   3      +5
        bool D_RF_ena;     

        void send();
        void setRegister();
        void WriteRegister32(const uint32_t value);
};

#endif

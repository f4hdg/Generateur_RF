/**********************************
 * Lib pour ADF4350
 * part Loic F4HDG
 * 
 * creation par le mixage des codes
 *   de Neal Pisenti, 2013. JQI - Strontium - UMD
 *   et Alain Fort F1CJN feb 2,2016
 * un grand merci a eux
 ***********************************/

#include "Arduino.h"
#include "SPI.h"
#include "ADF4350.h"

// Constructor function; initializes communication pinouts

ADF4350::ADF4350(int ssPin) {
  slaveSelectPin = ssPin;
  pinMode (slaveSelectPin, OUTPUT);
  digitalWrite(slaveSelectPin, HIGH);
}

// Initializes a new ADF4350 object, with refClk (in Mhz), and initial frequency.
void ADF4350::initialize(double freq, double refClk) {


  //Freq = 15000000;  //Startfrequenz generel 100Hz aulösung
  registers[0] =  0x4580A8;
  registers[1] =  0x80080C9;
  registers[2] =  0x4E42;
  registers[3] =  0x4B3;
  registers[4] =  0xBC803C;
  registers[5] =  0x580005;

//  setRegister();

  D_RF_ena = false;

  PFDRFfreq = 25;
  RFfreq = 1152;
  RFpow = 0;
  OutputChannelSpacing = 0.01; // Pas de fréquence = 10kHz
 
  PFDRFfreq = refClk;
  RFfreq = freq;
  
  ADF4350::setRegister();
  
  ADF4350::send();

}

void ADF4350::rfEnable(bool rf) {
  D_RF_ena = rf;
  ADF4350::setRegister();
  ADF4350::send();
}

void ADF4350::setFreq(double freq) {
  RFfreq = freq;
  ADF4350::setRegister();
  ADF4350::send();
}

// CAREFUL!!!! pow must be 0, 1, 2, or 3... corresponding to -4, -1, 3, 5 dbm.
void ADF4350::setRfPower(int pow) {
  if (pow < 0)
  {
    RFpow = 0;
  }
  else if (pow > 3)
  {
    RFpow = 3;
  }
  RFpow = pow;
  ADF4350::setRegister();
  ADF4350::send();
}


/////////////////////////////////////////////////////////////
// updates dynamic registers, and writes values to PLL board
void ADF4350::send() {
  ADF4350::setRegister();
  for (int i = 5; i >= 0; i--)  // programmation ADF4351 en commencant par R5
    WriteRegister32(registers[i]);
}

/////////////////////////////////////////////////////////////

void ADF4350::WriteRegister32(const uint32_t value)   //Programme un registre 32bits
{
  digitalWrite(slaveSelectPin, LOW);
  for (int i = 3; i >= 0; i--)          // boucle sur 4 x 8bits
  {
    SPI.transfer((value >> 8 * i) & 0xFF); // décalage, masquage de l'octet et envoi via SPI
  }
  digitalWrite(slaveSelectPin, HIGH);
  digitalWrite(slaveSelectPin, LOW);
}



//////////////////////////////////////////// REGISTER UPDATE FUNCTIONS
void ADF4350::setRegister() {
  double FRACF;
  unsigned int long INTA, FRAC, MOD;
  byte OutputDivider;

  if (RFfreq >= 2200) {
    OutputDivider = 1;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 2200) {
    OutputDivider = 2;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 1100) {
    OutputDivider = 4;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 550)  {
    OutputDivider = 8;
    bitWrite (registers[4], 22, 0);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 275)  {
    OutputDivider = 16;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 0);
  }
  if (RFfreq < 137.5) {
    OutputDivider = 32;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 0);
    bitWrite (registers[4], 20, 1);
  }
  if (RFfreq < 68.75) {
    OutputDivider = 64;
    bitWrite (registers[4], 22, 1);
    bitWrite (registers[4], 21, 1);
    bitWrite (registers[4], 20, 0);
  }

  INTA = (RFfreq * OutputDivider) / PFDRFfreq;
  MOD = (PFDRFfreq / OutputChannelSpacing);
  FRACF = (((RFfreq * OutputDivider) / PFDRFfreq) - INTA) * MOD;
  FRAC = round(FRACF); // On arrondit le résultat

  registers[0] = 0;
  registers[0] = INTA << 15; // OK
  FRAC = FRAC << 3;
  registers[0] = registers[0] + FRAC;

  registers[1] = 0;
  registers[1] = MOD << 3;
  registers[1] = registers[1] + 1 ; // ajout de l'adresse "001"
  bitSet (registers[1], 27); // Prescaler sur 8/9

  if (D_RF_ena)
    bitClear(registers[2], 5);
  else
    bitSet(registers[2], 5);

  bitSet (registers[2], 28); // Digital lock == "110" sur b28 b27 b26
  bitSet (registers[2], 27); // digital lock
  bitClear (registers[2], 26); // digital lock


  switch (RFpow)
  {
    case 0 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
      break;
    case 1 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 0);
      break;
    case 2 :
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 1);
      break;
    case 3 :
      bitWrite (registers[4], 3, 1);
      bitWrite (registers[4], 4, 1);
      break;
    default:
      bitWrite (registers[4], 3, 0);
      bitWrite (registers[4], 4, 0);
  }
}

/*
   Générateur SHF

   By : Loïc F4HDG  - 2019

   Materiel :
    - TFT 1.8"
    - Arduino Nano
    - ADF4351
    - Attenuateur PE4302
    - Amp
*/


/*
 *  calbage TFT
 *    TFT_CS        10
 *    TFT_RST        9
 *    TFT_DC         8
 *    TFT_SDA       11 
 *    TFT_SCL       13
 *    
 *    Prevoir pont resistif 470R (serie) 1k (vers masse)
 *    ADF4351_LE 7
 *    ADF4351_MOSI      11
 *    ADF4351_SCK       13    
 *       
 *    Encodeur_inter    3
 *    Encodeur_rot      4
 *    Encodeur_rot      5
 *    
 *    PE4302            A0->A5
 *    
 *    inter meenu       5 
 *    
 *    Si5351_SDA        A4      probleme avec att
 *    Si5351_SCL        A5      probleme avec att
 *    
 *          
 */

//#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include "ADF4350.h"
#include <Encoder.h>
#include <Adafruit_SI5351.h>



//#define DEBUG 0

///////////////////////////////////////////////////////////////////////////// TFT
#define TFT_CS        10                    // calbage TFT
#define TFT_RST        9                    // calbage TFT 
#define TFT_DC         8                    // calbage TFT

#define TFT_X           160                           // taille TFT
#define TFT_Y           128                           // taille TFT

#define MARGE           5                           // Marge X et Y
#define TITRE_X         MARGE + 20                  // titre en haut
#define TITRE_Y         MARGE                       // titre en haut
#define LIGNE_Y         MARGE + TITRE_Y + 3         // ligne sous titre

#define PARAM_1_X       MARGE                      // frequence X
#define PARAM_1_Y       MARGE + 10                 // frequence Y
#define PARAM_1_TAILLE  10

#define PARAM_2_X       MARGE                      // pas freq X
#define PARAM_2_Y       MARGE + 40                 // pas freq sortie Y
#define PARAM_2_TAILLE  10

#define PARAM_3_X       MARGE                      // attenuateur sortie X
#define PARAM_3_Y       MARGE + 60                 // attenuateur sortie Y
#define PARAM_3_TAILLE  10

#define PARAM_4_X       MARGE                      // pas attenuateur X
#define PARAM_4_Y       MARGE + 90                 // pas attenuateur Y
#define PARAM_4_TAILLE  10

#define PARAM_X_MAX     TFT_X - MARGE              // param X

#define PARAM_MAX       3                         // Numero du parametre maxi

#define MENU_LIGNE_Y    TFT_Y - MARGE*3           // Y de la ligne du menu
#define MENU_Y          MENU_LIGNE_Y + 3          // Y du menu
#define MENU_L          (TFT_X-MARGE)/4           // Largeur du menu
#define MENU_H          TFT_Y - MARGE             // Hauteur du menu
#define MENU_MAX        3                         // Numero du menu maxi

/// Creation TFT
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

///////////////////////////////////////////////////////////////////////////// ADF4350
#define ADF4351_LE 7                        // Cablage ADF4350
// Creation de la PLL
ADF4350 PLL_SHF(ADF4351_LE);
double frequence = 1151.00;                     // Frequence en MHz
// int    puissance = 0;                           // index de la puissance idem index text_puis et config lib
// String text_puis[4] = {"-4", "-1", "+2", "+5"}; // Text de la puissance ne fonction de index
double freq_pas[6] = {0.01, 0.1, 1, 10, 100, 1000}; // 1kHz
int    freq_pas_index = 1;
bool   sortie_RF = false;

double frequence_debut = 34;
double frequence_fin = 4400;
double freq_pas_balayage[7] = {0, 0.01, 0.1, 1, 10, 100, 1000}; // 1kHz
int    freq_pas_balayage_index = 0;
bool   mode_balayage = false;

///////////////////////////////////////////////////////////////////////////// SI5351
#define XTAL_FREQ 27000000              // Crystal frequency for Si5351A board - needs to be adjusted specific to your board's crystal
Adafruit_SI5351 PLL_RF = Adafruit_SI5351();



///////////////////////////////////////////////////////////////////////////// ATTENUATEUR
double att = 10;
double att_pas[4] = {0.5, 1, 5, 10}; // dB
int    att_pas_index = 1;
int    att_pin[6] = { A0, A1, A2, A3, A4, A5};

///////////////////////////////////////////////////////////////////////////// Encoder
#define CODER_INTER  3
#define INTER_REBOND 100
#define INTER_COURT  300
#define INTER_LONG  1500

Encoder myEnc(4, 5);
long myEnc_old;
int32_t tps_coder_bas = -1;
int coder_clic = 0;
long myEnc_value_old;

///////////////////////////////////////////////////////////////////////////// Config General de fonctionnement
#define TPS_LOOP 100
#define MENU_INTER  2
int32_t tps_old;
int     Param_select = 0;        // param 0 freq; 1 pas freq ; 2 puissance ;  3 pas
int     Menu_select = 0;         // menu  0 CW  ; 1 SWEEP    ; 2 Pilotage
bool    Mode_param = true;       // true : configure page    ; false : selec menu
int32_t tps_inter_menu_bas = -1;
/// un clic boutton menu passe ne en mode menu
/// un clic long bouton menu coupe la RF
int32_t tps_old_balayage;



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////// SETUP
void setup(void) {
  Serial.begin(115200);
  Serial.println("GRF-2k5-4400 - 2020");
  Serial.println("Port Pilotage.... OK");

#ifdef DEBUG
  Serial.println("Debug...");
#endif

  // config sortie interrupt de l'inter du l'encodeur ir
  pinMode(CODER_INTER, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(CODER_INTER), coder_inter, CHANGE);
  pinMode(MENU_INTER, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(MENU_INTER), menu_inter, CHANGE);
#ifdef DEBUG
  Serial.println("interrupt on ");
#endif

  tps_old = millis();

  tft.initR(INITR_BLACKTAB);                          // init l'ecran
  tft.setRotation(1);                                 // Tourne l'ecran mode paysage
  Generateur_TFT();
  Menu_TFT();

  /// Sortie pour l'attenuateur
  for (int i = 5; i >= 0; i--)          // boucle sur 4 x 8bits
  {
    pinMode(att_pin[i], OUTPUT);
  }
#ifdef DEBUG
  Serial.println("sortie att on ");
#endif

  /* Initialise the PLL */
  PLL_SHF.initialize(frequence, 25); // initialize the PLL to output 400 Mhz, using an
  PLL_SHF.rfEnable(false);

  if (PLL_RF.begin() != ERROR_NONE)
  {
    /* There was a problem detecting the IC ... check your connections */
    Serial.print("EE Si5351 : Check wire or I2C ADDR!");
    while (1);
  }
  PLL_RF.enableOutputs(false);

#ifdef DEBUG
  Serial.println("RF on ");
#endif
  tps_old_balayage = millis();


}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////// LOOP
void loop() {
  //////////////////////////////// ENCODER
  long myEnc_value = myEnc.read() / 4;
  long myEnc_pas ;
  myEnc_pas = myEnc_value_old - myEnc_value;

  if (((millis() - tps_old) > TPS_LOOP) && (myEnc_value_old - myEnc_value != 0))  // Reconfig affichage
  {
    if (Mode_param)
    {
      switch (Menu_select)
      {
        case 0:
          Generateur_Config(myEnc_pas);
          break;
        case 1 :
          Balayage_Config(myEnc_pas); // Balayage
          break;
        case 2 :
          // Pilotage
          break;
      }
    }
    else													// init affichage
    {
      /// Changement de page de parametre
      Menu_select = Menu_select + myEnc_pas;
      if (Menu_select < 0)
        Menu_select = 2;
      else if (Menu_select > 2)
        Menu_select = 0;

      switch (Menu_select)
      {
        case 0:
          Generateur_TFT();
          break;
        case 1 :
          Balayage_TFT();
          break;
        case 2 :
          Pilotage_TFT();
          break;
      }
      Menu_TFT();
    }
    myEnc_value_old = myEnc_value;
    tps_old = millis();
  }

  if (digitalRead(CODER_INTER) && coder_clic != 0)
  {
    if (Mode_param == true)
    {
      switch (Menu_select)
      {
        case 0:
          Generateur_Clic();
          break;
        case 1 :
          Balayage_Clic();// Balayage
          break;
        case 2 :
          // Pilotage pas de clic
          break;
      }
    }
    else /// mode menu
    {

    }
    coder_clic = 0;
  }

  ///////////// Boucle de Balayage
  if (((millis() - tps_old_balayage) > TPS_LOOP * 2) && (mode_balayage)) //// Boucle a 2*TPS_LOOP = 200ms
  {
    if (Menu_select == 0) {

      tft.setCursor(PARAM_1_X , PARAM_1_Y );
      tft.setTextColor(ST77XX_BLACK);
      tft.setTextSize(3);
      tft.println(frequence);
    }
    if (frequence_debut < frequence_fin)
    {
      frequence = frequence + freq_pas_balayage[freq_pas_balayage_index];
      if (frequence > frequence_fin)
        frequence = frequence_debut;
    }
    else
    {
      frequence = frequence - freq_pas_balayage[freq_pas_balayage_index];
      if (frequence < frequence_fin)
        frequence = frequence_debut;
    }

#ifdef DEBUG
    Serial.print("Balayage freq : ");
    Serial.println(frequence);
#endif

    if (Menu_select == 0) {
      tft.setCursor(PARAM_1_X , PARAM_1_Y );
      tft.setTextColor(ST77XX_RED);
      tft.setTextSize(3);
      tft.print(frequence);
      tft.setTextSize(1);
      tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
      tft.println(" MHz");
    }
    //PLL.setFreq(frequence);
    setFrequence();
    tps_old_balayage = millis();
  }

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Initialisation MENU
void Menu_TFT(void)
{
  int i;
  tft.drawLine(0, MENU_LIGNE_Y, TFT_X, MENU_LIGNE_Y, 0x7347);   // Ligne sous le titre
  tft.setTextSize(1);
  tft.setCursor(MARGE              + 2, MENU_Y + 2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("CW");
  tft.setCursor(MARGE + MENU_L     + 2, MENU_Y + 2);
  if ( mode_balayage)
    tft.setTextColor(ST77XX_GREEN);
  else
    tft.setTextColor(ST77XX_RED);
  tft.println("Balay.");
  tft.setCursor(MARGE + MENU_L * 2 + 2, MENU_Y + 2);
  tft.setTextColor(ST77XX_WHITE);
  tft.println("Pilot.");
  tft.setCursor(MARGE + MENU_L * 3 + 2, MENU_Y + 2);
  if (sortie_RF)
    tft.setTextColor(ST77XX_GREEN);
  else
    tft.setTextColor(ST77XX_RED);
  tft.println("Sortie");

  for (i = 0; i < 4; i++)
  {
    if (Menu_select == i && !Mode_param)
      tft.drawRect(MARGE + MENU_L * i, MENU_Y, MENU_L, MENU_H, ST77XX_GREEN);
    else
      tft.drawRect(MARGE + MENU_L * i, MENU_Y, MENU_L, MENU_H, 0x7347);
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Initialisation Ecran Generateur
void Generateur_TFT(void)
{
  // INIT TFT
  tft.fillScreen(ST77XX_BLACK);                       // vide en noir

  // tft.setRotation(1);                                 // Tourne l'ecran mode paysage
  tft.setCursor(TITRE_X , TITRE_Y );                  // Emplacement Titre
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(1);
  tft.println("- G-SHF-43-4400 -");                           // Texte titre
  tft.drawLine(0, LIGNE_Y, TFT_X, LIGNE_Y, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_1_X , PARAM_1_Y );                      // Affichage Frequence
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(3);
  tft.print(frequence);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
  tft.println(" MHz");

  tft.drawLine(0, PARAM_2_Y - 3 , TFT_X, PARAM_2_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_2_X , PARAM_2_Y );                      // Affichage PAS Frequence
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
  tft.println(freq_pas[freq_pas_index]);

  tft.drawLine(0, PARAM_3_Y - 3 , TFT_X, PARAM_3_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_3_X, PARAM_3_Y);                      // Affichage Puisance
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(3);
  tft.print(att);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
  tft.println(" dB");

  tft.drawLine(0, PARAM_4_Y - 3 , TFT_X, PARAM_4_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_4_X , PARAM_4_Y );                      // Affichage PAS Frequence
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(2);
  tft.println(att_pas[att_pas_index]);

  if (Mode_param) Param_curseur(ST77XX_GREEN);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  CONFIG du Generateur
void Generateur_Config(long myEnc_pas)
{

#ifdef DEBUG
  Serial.print("Encodeur tour : new + old");
  Serial.println(myEnc_value_old);
#endif

  switch (Param_select)
  {
    case 0 :    /////////////// MODIF FREQ
      if (!((frequence + (myEnc_pas * freq_pas[freq_pas_index])) > 4400) && !((frequence + (myEnc_pas * freq_pas[freq_pas_index])) < 34))
      {

        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_BLACK);
        tft.setTextSize(3);
        tft.println(frequence);
        frequence = frequence + (myEnc_pas * freq_pas[freq_pas_index]);
        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_RED);
        tft.setTextSize(3);
        tft.print(frequence);
        tft.setTextSize(1);
        tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
        tft.println(" MHz");
        //PLL.setFreq(frequence);
        setFrequence();
      }
      break;

    case 1 :    /////////////// MODIF PAS FREQ
      tft.setCursor(PARAM_2_X , PARAM_2_Y );
      tft.setTextColor(ST77XX_BLACK);
      tft.setTextSize(2);
      tft.println(freq_pas[freq_pas_index]);

      freq_pas_index = freq_pas_index + myEnc_pas ;
      if (freq_pas_index > 5) freq_pas_index = 5;
      else if (freq_pas_index < 0 ) freq_pas_index = 0;

      tft.setCursor(PARAM_2_X , PARAM_2_Y );
      tft.setTextColor(ST77XX_RED);
      tft.setTextSize(2);
      tft.println(freq_pas[freq_pas_index]);
      break;

    case 2 :    /////////////// MODIF ATTENUATEUR
      if (!((att  + (myEnc_pas * att_pas[att_pas_index])) > 31.5) && !((att  + (myEnc_pas * att_pas[att_pas_index])) < 0))
      {
        tft.setCursor(PARAM_3_X, PARAM_3_Y);
        tft.setTextColor(ST77XX_BLACK);
        tft.setTextSize(3);
        tft.print(att);
        tft.setTextSize(1);
        tft.println(" dB");

        att = att  + (myEnc_pas * att_pas[att_pas_index]);
        int valeur;
        if (att - int(att) > 0)
        {
          valeur = (int(att) * 2) + 1;
        }
        else
        {
          valeur = (int(att) * 2);
        }
        for (int i = 5; i >= 0; i--)          // boucle sur 4 x 8bits
        {
          if (((valeur >> 1 * i) & 0x01))
            digitalWrite(att_pin[i], HIGH);
          else
            digitalWrite(att_pin[i], LOW);
        }

        tft.setCursor(PARAM_3_X, PARAM_3_Y);                      // Affichage Puisance
        tft.setTextColor(ST77XX_BLUE);
        tft.setTextSize(3);
        tft.print(att);
        tft.setTextSize(1);
        tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
        tft.println(" dB");
      }
      break;

    case 3 :    /////////////// MODIF PAS ATTENNUATEUR
      tft.setCursor(PARAM_4_X , PARAM_4_Y );                      // Affichage PAS Frequence
      tft.setTextColor(ST77XX_BLACK);
      tft.setTextSize(2);
      tft.println(att_pas[att_pas_index]);

      att_pas_index = att_pas_index + myEnc_pas ;
      if (att_pas_index > 3) att_pas_index = 3;
      else if (att_pas_index < 0 ) att_pas_index = 0;

      tft.setCursor(PARAM_4_X , PARAM_4_Y );                      // Affichage PAS Frequence
      tft.setTextColor(ST77XX_BLUE);
      tft.setTextSize(2);
      tft.println(att_pas[att_pas_index]);
      break;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Generateur CLIC
void Generateur_Clic()
{
  switch (coder_clic)
  {
    case 1 :  /// clic court
      Param_curseur(ST77XX_BLACK);
      switch (Param_select)
      {
        case 0 :
          Param_select = 1;
          break;
        case 1 :
          Param_select = 0;
          break;
        case 2 :
          Param_select = 3;
          break;
        case 3 :
          Param_select = 2;
          break;
      }
      Param_curseur(ST77XX_GREEN);
      break;
    case 2 :   /// clic long
      Param_curseur(ST77XX_BLACK);
      switch (Param_select)
      {
        case 0 :
          Param_select = 2;
          break;
        case 1 :
          Param_select = 2;
          break;
        case 2 :
          Param_select = 0;
          break;
        case 3 :
          Param_select = 0;
          break;
      }
      Param_curseur(ST77XX_GREEN);
      break;
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Initialisation Ecran Generateur
void Balayage_TFT(void)
{
  // INIT TFT
  // tft.initR(INITR_BLACKTAB);                          // init l'ecran
  tft.fillScreen(ST77XX_BLACK);                       // vide en noir

  tft.setCursor(TITRE_X , TITRE_Y );                  // Emplacement Titre
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(1);
  tft.println("- G-SHF-43-4400 -");                           // Texte titre
  tft.drawLine(0, LIGNE_Y, TFT_X, LIGNE_Y, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_1_X , PARAM_1_Y );                      // Affichage Frequence de depart
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(3);
  tft.print(frequence_debut);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
  tft.println(" MHz");

  tft.drawLine(0, PARAM_2_Y - 3 , TFT_X, PARAM_2_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_2_X , PARAM_2_Y );                      // Affichage PAS Frequence de réglege
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(2);
  tft.println(freq_pas[freq_pas_index]);

  tft.drawLine(0, PARAM_3_Y - 3 , TFT_X, PARAM_3_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_3_X, PARAM_3_Y);                      // Affichage Puisance
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(3);
  tft.print(frequence_fin);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
  tft.println(" MHz");

  tft.drawLine(0, PARAM_4_Y - 3 , TFT_X, PARAM_4_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_4_X , PARAM_4_Y );                      // Affichage PAS Frequence
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(2);
  tft.println(freq_pas_balayage[freq_pas_balayage_index]);

  if (Mode_param) Param_curseur(ST77XX_GREEN);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  CONFIG du Balayage
void Balayage_Config(long myEnc_pas)
{

#ifdef DEBUG
  Serial.print("Encodeur tour : new + old");
  Serial.println(myEnc_value_old);
#endif

  switch (Param_select)
  {
    case 0 :    /////////////// MODIF FREQ de debut
      if (!((frequence_debut + (myEnc_pas * freq_pas[freq_pas_index])) > 4400) && !((frequence_debut + (myEnc_pas * freq_pas[freq_pas_index])) < 34))
      {

        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_BLACK);
        tft.setTextSize(3);
        tft.println(frequence_debut);
        frequence_debut = frequence_debut + (myEnc_pas * freq_pas[freq_pas_index]);
        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_RED);
        tft.setTextSize(3);
        tft.print(frequence_debut);
        tft.setTextSize(1);
        tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
        tft.println(" MHz");
        //PLL.setFreq(frequence_debut);
      }
      break;

    case 1 :    /////////////// MODIF PAS FREQ
      tft.setCursor(PARAM_2_X , PARAM_2_Y );
      tft.setTextColor(ST77XX_BLACK);
      tft.setTextSize(2);
      tft.println(freq_pas[freq_pas_index]);

      freq_pas_index = freq_pas_index + myEnc_pas ;
      if (freq_pas_index > 5) freq_pas_index = 5;
      else if (freq_pas_index < 0 ) freq_pas_index = 0;

      tft.setCursor(PARAM_2_X , PARAM_2_Y );
      tft.setTextColor(ST77XX_RED);
      tft.setTextSize(2);
      tft.println(freq_pas[freq_pas_index]);
      break;

    case 2 :    /////////////// MODIF frequence de fin
      if (!((frequence_fin  + (myEnc_pas * freq_pas[freq_pas_index])) > 4400) && !((frequence_fin + (myEnc_pas * freq_pas[freq_pas_index])) < 34))
      {
        tft.setCursor(PARAM_3_X, PARAM_3_Y);
        tft.setTextColor(ST77XX_BLACK);
        tft.setTextSize(3);
        tft.print(frequence_fin);
        tft.setTextSize(1);
        tft.println(" MHz");
        frequence_fin = frequence_fin  + (myEnc_pas * freq_pas[freq_pas_index]);
        tft.setCursor(PARAM_3_X, PARAM_3_Y);
        tft.setTextColor(ST77XX_RED);
        tft.setTextSize(3);
        tft.print(frequence_fin);
        tft.setTextSize(1);
        tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
        tft.println(" MHz");
      }
      break;

    case 3 :    /////////////// MODIF PAS de balayage
      tft.setCursor(PARAM_4_X , PARAM_4_Y );
      tft.setTextColor(ST77XX_BLACK);
      tft.setTextSize(2);
      tft.println(freq_pas_balayage[freq_pas_balayage_index]);

      freq_pas_balayage_index = freq_pas_balayage_index + myEnc_pas ;
      if (freq_pas_balayage_index > 6) freq_pas_balayage_index = 6;
      else if (freq_pas_balayage_index < 0 ) freq_pas_balayage_index = 0;
      if (freq_pas_balayage_index == 0) mode_balayage = false;
      else mode_balayage = true;
      tft.setCursor(PARAM_4_X , PARAM_4_Y );                      // Affichage PAS Frequence
      tft.setTextColor(ST77XX_BLUE);
      tft.setTextSize(2);
      tft.println(freq_pas_balayage[freq_pas_balayage_index]);
      Menu_TFT();
      break;
  }
  if (frequence < frequence_debut || frequence > frequence_fin)
    frequence = frequence_debut;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Balayage CLIC
void Balayage_Clic()
{
  switch (coder_clic)
  {
    case 1 :  /// clic court
      Param_curseur(ST77XX_BLACK);
      switch (Param_select)
      {
        case 0 :
          Param_select = 1;
          break;
        case 1 :
          Param_select = 2;
          break;
        case 2 :
          Param_select = 0;
          break;
          //        case 3 :
          //          Param_select = 2;
          //          break;
      }
      Param_curseur(ST77XX_GREEN);
      break;
    case 2 :   /// clic long
      Param_curseur(ST77XX_BLACK);
      switch (Param_select)
      {
        case 0 :
          Param_select = 3;
          break;
        case 1 :
          Param_select = 3;
          break;
        case 2 :
          Param_select = 3;
          break;
        case 3 :
          Param_select = 0;
          break;
      }
      Param_curseur(ST77XX_GREEN);
      break;
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Initialisation Ecran Pilotage
void Pilotage_TFT(void)
{
  // INIT TFT
  tft.fillScreen(ST77XX_BLACK);                       // vide en noir

  // tft.setRotation(1);                                 // Tourne l'ecran mode paysage
  tft.setCursor(TITRE_X , TITRE_Y );                  // Emplacement Titre
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(1);
  tft.println("- G-SHF-43-4400 -");                           // Texte titre
  tft.drawLine(0, LIGNE_Y, TFT_X, LIGNE_Y, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_1_X , PARAM_1_Y );                      // Affichage Frequence
  tft.setTextColor(ST77XX_RED);
  tft.setTextSize(3);
  tft.print(frequence);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
  tft.println(" MHz");

  tft.drawLine(0, PARAM_3_Y - 3 , TFT_X, PARAM_3_Y - 3, 0x7347);   // Ligne sous le titre

  tft.setCursor(PARAM_3_X, PARAM_3_Y);                      // Affichage Puisance
  tft.setTextColor(ST77XX_BLUE);
  tft.setTextSize(3);
  tft.print(att);
  tft.setTextSize(1);
  tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
  tft.println(" dB");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  CONFIG du Pilotage
void Pilotage_Config(String cmd, double eff_val)
{

#ifdef DEBUG
  Serial.print("Encodeur tour : new + old");
  Serial.println(myEnc_value_old);
#endif


  if (cmd == "freq")	/////////////// MODIF FREQ
  {

    //PLL.setFreq(frequence);
    setFrequence();
    tft.setCursor(PARAM_1_X , PARAM_1_Y );
    tft.setTextColor(ST77XX_BLACK);
    tft.setTextSize(3);
    tft.println(eff_val);
    tft.setCursor(PARAM_1_X , PARAM_1_Y );
    tft.setTextColor(ST77XX_RED);
    tft.setTextSize(3);
    tft.print(frequence);
    tft.setTextSize(1);
    tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
    tft.println(" MHz");
  }

  else if (cmd == "att")    /////////////// MODIF ATTENUATEUR
  {
    tft.setCursor(PARAM_3_X, PARAM_3_Y);
    tft.setTextColor(ST77XX_BLACK);
    tft.setTextSize(3);
    tft.print(eff_val);
    tft.setTextSize(1);
    tft.println(" dB");
    tft.setCursor(PARAM_3_X, PARAM_3_Y);                      // Affichage Puisance
    tft.setTextColor(ST77XX_BLUE);
    tft.setTextSize(3);
    tft.print(att);
    tft.setTextSize(1);
    tft.setCursor(TFT_X - 30, PARAM_3_Y + 10);
    tft.println(" dB");
  }

}



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  Dessins du Curseur param du generateur
void Param_curseur(uint16_t couleur)
{
  switch (Param_select)
  {
    case 0 :
      tft.fillTriangle(PARAM_X_MAX - PARAM_1_TAILLE, PARAM_1_Y + PARAM_1_TAILLE / 2, PARAM_X_MAX,  PARAM_1_Y + PARAM_1_TAILLE, PARAM_X_MAX,  PARAM_1_Y, couleur);
      break;
    case 1 :
      tft.fillTriangle(PARAM_X_MAX - PARAM_2_TAILLE, PARAM_2_Y + PARAM_2_TAILLE / 2, PARAM_X_MAX,  PARAM_2_Y + PARAM_2_TAILLE, PARAM_X_MAX,  PARAM_2_Y, couleur);
      break;
    case 2 :
      tft.fillTriangle(PARAM_X_MAX - PARAM_3_TAILLE, PARAM_3_Y + PARAM_3_TAILLE / 2, PARAM_X_MAX,  PARAM_3_Y + PARAM_3_TAILLE, PARAM_X_MAX,  PARAM_3_Y, couleur);
      break;
    case 3 :
      tft.fillTriangle(PARAM_X_MAX - PARAM_4_TAILLE, PARAM_4_Y + PARAM_4_TAILLE / 2, PARAM_X_MAX,  PARAM_4_Y + PARAM_4_TAILLE, PARAM_X_MAX,  PARAM_4_Y, couleur);
      break;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  INTERRUPT CODER
void coder_inter()
{

#ifdef DEBUG
  Serial.println("Encodeur clic int...");
#endif

  if (!digitalRead(CODER_INTER) && tps_coder_bas == -1)
  {
    tps_coder_bas = millis();
    coder_clic = 0;
  }
  else if (digitalRead(CODER_INTER))
  {

#ifdef DEBUG
    Serial.println("Encodeur clic int haut...");
#endif

    if ((millis() - tps_coder_bas > INTER_REBOND) && (millis() - tps_coder_bas < INTER_COURT))
    {
      coder_clic = 1;
    }
    else if ((millis() - tps_coder_bas > INTER_REBOND) && (millis() - tps_coder_bas < INTER_LONG))
    {
      coder_clic = 2;
    }
    tps_coder_bas = -1;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  INTERRUPT MENU Bouton
void menu_inter()
{

#ifdef DEBUG
  Serial.println("Menu clic int...");
#endif

  if (!digitalRead(MENU_INTER) && tps_inter_menu_bas == -1)
  {
    tps_inter_menu_bas = millis();
  }
  else if (digitalRead(MENU_INTER))
  {

#ifdef DEBUG
    Serial.println("Inter menu clic int haut...");
#endif

    if ((millis() - tps_inter_menu_bas > INTER_REBOND) && (millis() - tps_inter_menu_bas < INTER_COURT))
    {
      if (Mode_param) {
        Mode_param = false;
        Param_curseur(ST77XX_BLACK);
      }
      else {
        Mode_param = true;
        Param_curseur(ST77XX_GREEN);
      }
      Menu_TFT();

#ifdef DEBUG
      Serial.print(" Clic menu : court");
#endif

    }
    else if ((millis() - tps_inter_menu_bas > INTER_REBOND) && (millis() - tps_inter_menu_bas < INTER_LONG))
    {
      if (sortie_RF) sortie_RF = false;
      else sortie_RF = true;
      Menu_TFT();
#ifdef DEBUG
      Serial.print(" Clic menu : long");
#endif
    }

    tps_inter_menu_bas = -1;
  }

}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////  PILOTAGE
//void Pilotage()
void serialEvent()
{

  Menu_select = 2;
  Menu_TFT();
  String instruction;
  instruction = Serial.readStringUntil(';');
  //Serial.println(instruction);

  if (instruction == "\n")
  {
    //   Serial.print("sans instruction");
    //   Serial.println(instruction);

  }
  else {
    //Serial.print("Question");
    //Serial.println(instruction);

    /*  Fonction            demande        config
        frequence debut     freqdebut?;   freqdebut FREQ_MHZ;
        frequance fin       freqfin?;     freqfin   FREQ_MHZ;
        pas de freq         freqpas?;     ;
        frequence en cours  freq?;        freq      FREQ_MHZ;
        attenuateur         att?;         att       VALUE_DB; /!\ pas de 0.5
        attenuateur pas     attpas?;
        balayage            balayage?;    balayage  1:0;    1=true:0=false
        sortierf            sortierf?;    sortierf  1:0;    1=true:0=false
    */
    instruction.toLowerCase();

    if (instruction.indexOf("?") >= 0)   //// Demande d'une valeur
    {
      instruction.remove(instruction.indexOf("?"));     // supression du ?

      if (instruction.equalsIgnoreCase("freqdebut"))
      {
        Serial.print(frequence_debut);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("freqfin"))
      {
        Serial.print(frequence_fin);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("freqpas"))
      {
        Serial.print(freq_pas_balayage[freq_pas_balayage_index]);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("freq"))
      {
        Serial.print(frequence);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("att"))
      {
        Serial.print(att);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("attpas"))
      {
        Serial.print(att_pas[att_pas_index]);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("balayage"))
      {
        Serial.print(mode_balayage);
        Serial.print(";");
      }
      else if (instruction.equalsIgnoreCase("sortierf"))
      {
        Serial.print(sortie_RF);
        Serial.print(";");
      }
      else
      {
        Serial.println("Erreur:cmd?;"); // commande inconnue
      }

    }
    else  /// configuration
    {

      //      Serial.print("config");
      //      Serial.println(instruction);
      //  Serial.println(instruction.compareTo("freqdebut "));
      String instruction_val = instruction;
      bool change = false;
      instruction_val.remove(0, instruction_val.indexOf(" ") + 1);
      instruction.remove(instruction.indexOf(" "));
#ifdef DEBUG
      Serial.println(instruction);
      Serial.println(instruction_val);
#endif

      if (instruction == "freqdebut")
      {
        //instruction.remove(instruction.indexOf("freqdebut "), 10);
        frequence_debut = instruction_val.toDouble();
        if (frequence_debut > 4400)
          frequence_debut = 4400;
        else if (frequence_debut < 34)
          frequence_debut = 34;
      }
      else if (instruction == "freqfin")
      {
        //instruction.remove(instruction.indexOf("freqfin"), 8);
        frequence_fin = instruction_val.toDouble();
        if (frequence_fin > 4400)
          frequence_fin = 4400;
        else if (frequence_fin < 34)
          frequence_fin = 34;
      }
      /*else if (instruction.equalsIgnoreCase("freqpas"))
        {
        instruction.remove(instruction.indexOf("freqpas"), 7);
        frequence_debut(instruction.toDouble());

        Serial.print(freq_pas_balayage[freq_pas_balayage_index]);
        Serial.println(";");
        }*/
      else if (instruction == "freq")
      {
        //instruction.remove(instruction.indexOf("freq "), 5);
        // Serial.println(instruction);
        double eff_val = frequence;
        frequence = instruction_val.toDouble();
        // Serial.println(instruction_val.toDouble());
        if (frequence > 4400)
          frequence = 4400;
        else if (frequence < 34)
          frequence = 34;

        //PLL.setFreq(frequence);
        setFrequence();
        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_BLACK);
        tft.setTextSize(3);
        tft.println(eff_val);
        tft.setCursor(PARAM_1_X , PARAM_1_Y );
        tft.setTextColor(ST77XX_RED);
        tft.setTextSize(3);
        tft.print(frequence);
        tft.setTextSize(1);
        tft.setCursor(TFT_X - 30, PARAM_1_Y + 10);
        tft.println(" MHz");

        //Pilotage_Config("freq", eff_val);
      }
      else if (instruction == "att")
      {
        double eff_val = att;
        //instruction.remove(instruction.indexOf("att "), 4);
        att = (instruction_val.toDouble());
        if (att > 31)
          att = 31;
        else if (att < 0)
          att = 0;

        Pilotage_Config("att", eff_val);

        int valeur;
        if (att - int(att) > 0)
        {
          valeur = (int(att) * 2) + 1;
        }
        else
        {
          valeur = (int(att) * 2);
        }
        for (int i = 5; i >= 0; i--)          // boucle sur 4 x 8bits
        {
          if (((valeur >> 1 * i) & 0x01))
            digitalWrite(att_pin[i], HIGH);
          else
            digitalWrite(att_pin[i], LOW);
        }
      }
      /*    else if (instruction.equalsIgnoreCase("attpas"))
          {
            instruction.remove(instruction.indexOf("freqdebut"), 9);
            frequence_debut(instruction.toDouble());

            Serial.print(att_pas[att_pas_index]);
            Serial.println(";");
          }*/
      else if (instruction == "balayage")
      {
        //instruction.remove(instruction.indexOf("balayage "), 9);
        mode_balayage = (instruction_val.toInt());
      }
      else if (instruction == "sortierf")
      {
        //instruction.remove(instruction.indexOf("sortierf "), 9);
        sortie_RF = (instruction_val.toInt());
        Menu_TFT();
      }
      else
      {
        //   Serial.println(instruction);
        Serial.println("Erreur:cmd;");    // inconnue
      }

    }
  }
  //Serial.read();

}






///////////////////////////////////////////////// Envoi de la fréquence au PLL
void setFrequence()
{

  if (frequence < 34)
  {

    ////////////////////// SI5351

    uint32_t pllFreq;
    uint32_t l;
    float f;
    uint8_t mult;
    uint32_t num;
    uint32_t denom;
    uint32_t divider;

#ifdef  DEBUG_FREQ_CALC
    double freq_test;
#endif

    divider = 900000000 / (frequence * 1000000);        // Calculate the division ratio. 900,000,000 is the maximum internal PLL frequency: 900MHz
    if (divider % 2) divider--;        // Ensure an even integer division ratio

    pllFreq = divider * (frequence * 1000000);     // Calculate the pllFrequency: the divider * desired output frequency

    mult = pllFreq / XTAL_FREQ;          // Determine the multiplier to get to the required pllFrequency
    l = pllFreq % uint32_t(XTAL_FREQ);                 // It has three parts:
    f = l;                              // mult is an integer that must be in the range 15..90

    f *= 1048575;                       // num and denom are the fractional parts, the numerator and denominator
    f /= XTAL_FREQ;                      // each is 20 bits (range 0..1048575)
    num = f;                            // the actual multiplier is mult + num / denom
    denom = 1048575;                    // For simplicity we set the denominator to the maximum 1048575

#ifdef DEBUG_FREQ_CALC
    Serial.print("deviseur : ");
    Serial.println(divider);
    Serial.print("PLLfreq : ");
    Serial.println(pllFreq);
    Serial.print("multi : ");
    Serial.println(mult);
    Serial.print("f : ");
    Serial.println(f);
    Serial.print("num : ");
    Serial.println(num);
    Serial.print("denum : ");
    Serial.println(denom);
#endif

    PLL_RF.setupPLL(SI5351_PLL_A, mult, num, denom);
    //  Serial.print("Set Output #0 : ");
    //  Serial.println((XTAL_FREQ * (mult + (float(num) / float(denom)))) / divider);
    PLL_RF.setupMultisynth(0, SI5351_PLL_A, divider, 0, 1);
  }
  else
    PLL_SHF.setFreq(frequence);

}

# GRF-34-4400
Ce generateur permet la générateur de signaux RF de 34 MHz à 4400 GHz en mode CW ou sweep.

## Materiel
 * Arduino nano
 * ADF4350 PLL 34MHz-4400MHz
 * PE4302  Attenuateur 31,5 dB
 * Ampli 50MHz-6GHz
 * TFT
 * Encoder rotatif  
 

## Pilotage
Il est possible de communiquer avec le generateur via un port serie sur les broche **A6** (RX) et **A7** (TX)
Les instructions sont les suivance 

| Fonction           |  demande     |config                              |
|--------------------|--------------|------------------------------------|
| frequence debut    |  freqdebut?; |  freqdebut FREQ_MHZ;               |
| frequance fin      |  freqfin?;   |  freqfin   FREQ_MHZ;               |
| pas de freq        |  freqpas?;   |  N/A                               |
| frequence en cours |  freq?;      |  freq      FREQ_MHZ;               |
| attenuateur        |  att?;       |  att       VALUE_DB; /!\ pas de 0.5|
| attenuateur pas    |  attpas?;    |  N/A                               |
| balayage           |  balayage?;  |  balayage  1:0;    1=true:0=false  |
| sortierf           |  sortierf?;  |  sortierf  1:0;    1=true:0=false  |




